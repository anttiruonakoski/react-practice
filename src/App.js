import React from 'react';
import PropTypes from 'prop-types';
import { Box } from 'react-bulma-components/full';
import { Heading } from 'react-bulma-components/full';
import { Level } from 'react-bulma-components/full';
import { Container } from 'react-bulma-components/full';
import { Section } from 'react-bulma-components/full';
import { Button } from "react-bulma-components/full";
import AnimalButton from './components/AnimalButton';

class App extends React.Component {
  constructor(props) {
    super(props)
      this.state = {
        animals: ['kettu', 'susi', 'kärppä', 'pöllö', 'saukko'],
        adjectives: ['pörröinen', 'viekas', 'nopea', 'viisas', 'liukas'],
        chosenanimal: 'kettu',
        chosenadjective: 'pörröinen'
      }
  }

  render() {

    const rollAnimal = () => {
      let numero = Math.floor(Math.random()*this.state.animals.length)
      console.log('rollattu el', numero)
      this.setState({ chosenanimal: this.state.animals[numero]}) 
    }

    const rollAdjective = () => {
      let numero = Math.floor(Math.random()*this.state.adjectives.length)
      console.log('rollattu ad', numero)
      this.setState({ chosenadjective: this.state.adjectives[numero]}) 
    }

    const chosenanimal = this.state.chosenanimal
    const chosenadjective = this.state.chosenadjective

    AnimalButton.propTypes = {
      animal: PropTypes.string.isRequired,
      className: PropTypes.string.isRequired,
      children: PropTypes.node.isRequired,
    };

    return (
        <div>
          <Section>
            <Container>
                <Heading>Eläin arvonta</Heading>
                  <Level>
                    <Button color="info" animalname={chosenanimal} animaladjective={chosenadjective} renderAs={AnimalButton} roll={rollAnimal}>
                      Päivän eläin on 
                    </Button>
                    <Button color="primary" animalname={chosenanimal} animaladjective={chosenadjective} renderAs={AnimalButton} roll={rollAdjective}>
                      Huomisen eläin on 
                    </Button>
                    <Button color="inactive" animalname={chosenanimal} animaladjective={chosenadjective} renderAs={AnimalButton}>
                      Ylihuomisen eläin on 
                    </Button>
                  </Level>
            </Container>
          </Section>
        </div>
    )
  }
}

export default App;
