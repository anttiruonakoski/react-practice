import React from 'react'

const AnimalButton = ({ animalname, animaladjective, children, className, roll }) => {
  return (
    <a className={className} onClick={roll}>
      {children} {animalname} {animaladjective} 
    </a>
  )
}

export default AnimalButton